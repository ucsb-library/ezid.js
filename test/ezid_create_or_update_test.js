const FSPersister = require("@pollyjs/persister-fs")
const NodeHttpAdapter = require("@pollyjs/adapter-node-http")
const path = require("path")
const { Polly, setupMocha: setupPolly } = require("@pollyjs/core")
Polly.register(NodeHttpAdapter);
Polly.register(FSPersister);

const assert = require('assert')
const EzID = require('../lib/ezid.js')

const client = new EzID(
  { username: 'apitest',
    password: process.env.EZID_PASS,
    shoulder: 'ark:/99999/fk4' }
)

const original = {
  id: 'ark:/99999/fk4ucsbt0',
  metadata: {
    'dc.title': 'test thing',
    '_profile': 'dc'
  }
}

const updated = {
  id: 'ark:/99999/fk4ucsbt0',
  metadata: {
    'dc.title': 'real thing',
    'dc.publisher': 'real deal',
    '_profile': 'dc'
  }
}

describe('EzID#createOrUpdate', function () {
  setupPolly({
    mode: process.env.VCR_MODE || 'replay',

    adapters: ["node-http"],
    persister: "fs",
    persisterOptions: {
      fs: {
        recordingsDir: path.resolve(__dirname, "recordings")
      }
    },
    recordFailedRequests: true
  })

  it('should successfully create an ARK, then update it', function () {
    const { server } = this.polly;
    server.any().on('beforePersist', (req, recording) => {
      recording.request.headers = recording.request.headers.filter(({ name }) => name !== 'authorization')
    })

    return client.createOrUpdate(original).then(response => {
      assert.equal(response.id, original.id)

      return client.createOrUpdate(updated).then(response => {
        assert.equal(response.id, updated.id)

        return EzID.get(updated.id).then(info => {
          assert.deepEqual(
            info,
            {
              id: updated.id,
              metadata: Object.assign(
                updated.metadata, {
                  "_created": "1642635122",
                  "_export": "yes",
                  "_owner": "apitest",
                  "_ownergroup": "apitest",
                  "_status": "public",
                  "_target": "https://ezid.cdlib.org/id/ark:/99999/fk4ucsbt0",
                  "_updated": "1642639829",
                }
              )
            })
        })
      })
    })
  })
})
