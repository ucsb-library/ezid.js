const FSPersister = require("@pollyjs/persister-fs")
const NodeHttpAdapter = require("@pollyjs/adapter-node-http")
const path = require("path")
const { Polly, setupMocha: setupPolly } = require("@pollyjs/core")
Polly.register(NodeHttpAdapter);
Polly.register(FSPersister);

const assert = require('assert')
const EzID = require('../lib/ezid.js')

const client = new EzID(
  { username: 'apitest',
    password: process.env.EZID_PASS,
    shoulder: 'ark:/99999/fk4' }
)

describe('EzID#create, #update', function () {
  setupPolly({
    mode: process.env.VCR_MODE || 'replay',

    adapters: ["node-http"],
    persister: "fs",
    persisterOptions: {
      fs: {
        recordingsDir: path.resolve(__dirname, "recordings")
      }
    },
    recordFailedRequests: true
  })

  it('should successfully create an ARK, then update it', function () {
    const { server } = this.polly;
    server.any().on('beforePersist', (req, recording) => {
      recording.request.headers = recording.request.headers.filter(({ name }) => name !== 'authorization')
    })

    const original = {
      id: 'ark:/99999/fk4ucsb5',
      metadata: {
        'dc.title': 'test thing',
        '_profile': 'dc',
        '_status': 'reserved'
      }
    }

    const updated = {
      id: 'ark:/99999/fk4ucsb5',
      metadata: {
        'dc.title': 'real thing',
        '_profile': 'dc'
      }
    }

    return client.create(original).then(response => {
      assert.equal(response.id, original.id)

      return client.update(updated).then(response => {
        assert.equal(response.id, updated.id)

        return EzID.get(updated.id).then(info => {
          assert.deepEqual(
            info,
            {
              id: updated.id,
              metadata: Object.assign(
                updated.metadata, {
                  "_created": "1642639925",
                  "_export": "yes",
                  "_owner": "apitest",
                  "_ownergroup": "apitest",
                  "_status": "reserved",
                  "_target": "https://ezid.cdlib.org/id/ark:/99999/fk4ucsb5",
                  "_updated": "1642639925",
                }
              )
            }
          )
        })
      })
    })
  })

  it('should fail to update a nonexistent object', function () {
    const { server } = this.polly;
    server.any().on('beforePersist', (req, recording) => {
      recording.request.headers = recording.request.headers.filter(({ name }) => name !== 'authorization')
    })

    const fake = {
      id: 'ark:/99999/fk4ucsbno',
      metadata: {
        'dc.title': 'fake thing',
        '_profile': 'dc'
      }
    }

    return client.update(fake).catch(err => {
      assert.equal(err.statusCode, '400')
    })
  })
})
