const FSPersister = require("@pollyjs/persister-fs")
const NodeHttpAdapter = require("@pollyjs/adapter-node-http")
const path = require("path")
const { Polly, setupMocha: setupPolly } = require("@pollyjs/core")
Polly.register(NodeHttpAdapter);
Polly.register(FSPersister);

const assert = require('assert')
const EzID = require('../lib/ezid.js')

describe('EzID.get', function () {
  setupPolly({
    mode: process.env.VCR_MODE || 'replay',

    adapters: ["node-http"],
    persister: "fs",
    persisterOptions: {
      fs: {
        recordingsDir: path.resolve(__dirname, "recordings")
      }
    },
    recordFailedRequests: true
  })

  it('should return the metadata for a valid query', function () {
    const { server } = this.polly;
    server.any().on('beforePersist', (req, recording) => {
      recording.request.headers = recording.request.headers.filter(({ name }) => name !== 'authorization')
    })

    return EzID.get('ark:/48907/f3zc8132').then(info => {
      assert.deepEqual(
        info,
        {
          id: 'ark:/48907/f3zc8132',
          metadata: {
            '_created': '1449066251',
            'erc.ark': 'ark:/48907/f3zc8132',
            'erc.what': 'Carl R. Rogers collection',
            'erc.who': 'Rogers, Carl R. (Carl Ransom), 1902-1987',
            _export: 'yes',
            _ownergroup: 'sb-library',
            _owner: 'sb-adrl',
            _profile: 'erc',
            _status: 'public',
            _target: 'http://alexandria.ucsb.edu/lib/ark:/48907/f3zc8132',
            '_updated': '1477089288'
          }
        }
      )
    })
  })
})
