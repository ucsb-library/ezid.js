# Unreleased

# 1.0.0

- relicense under MIT
- run tests against LTS node
- replace HTTP mocking library `replayer` with PollyJS
- update dependencies

# 0.3.1

- move repository to gitlab

# 0.3.0

- replace `got` with `node-fetch`

# 0.2.0

- Switch to more structured metadata hashes, with `id` kept separate
  from the metadata used in EzID request bodies:
    ```
    {
      id: 'ark:/99999/fk4/ucsbcreate',
      metadata: {
        '_profile': 'erc',
        'erc.who': 'a bird'
      }
    }
    ```

- Document all external `EzID` functions in the README.

# 0.1.0

- Initial release with support for ARKs.
