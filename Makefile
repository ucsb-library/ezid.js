.PHONY: test

test:
	VCR_MODE=replay npm test

html:
	node_modules/.bin/jsdoc lib -r -d html
